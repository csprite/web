import lume from "lume/mod.ts";
import date from "lume/plugins/date.ts"; // Handling Date Formats
import feed from "lume/plugins/feed.ts"; // RSS & JSON Feeds
import slugifyUrls from "lume/plugins/slugify_urls.ts"; // Slugify URLS
import codeHighlight from "lume/plugins/code_highlight.ts"; 

import minify from 'npm:@node-minify/core';
import htmlMinifier from 'npm:@node-minify/html-minifier';
import cleanCSS from 'npm:@node-minify/clean-css';
import terser from 'npm:@node-minify/terser';

// Set DENO_ENV="production" when deploying in production

const site = lume({
	prettyUrls: false,
	server: {
		port: 4000
	}
});

site
	.ignore("README.md")
	.remoteFile("/share/css/syntax.light.css", "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/styles/atom-one-light.min.css")
	.remoteFile("/share/css/syntax.dark.css", "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/styles/atom-one-dark.min.css")
	.loadAssets([".css", ".js"])
	.copy("media", "media")
	.copy("assets", "assets")
	.use(date({
		formats: {
			"READABLE": "do MMM, yyyy"
		}
	}))
	.use(codeHighlight())
	.use(slugifyUrls())
	.use(feed({
		output: ["/feed.xml", "/feed.json"],
		query: "type=post",
		info: {
			title: "=site.title",
			description: "=site.description",
			lang: "=metas.lang",
			generator: true
		},
		items: {
			title: "=title",
			description: "=excerpt",
			published: "=date",
			lang: "=metas.lang"
		}
	}))
	.process([".html"], async (pages) => {
		// Remove hljs theme css if page doesn't use it.
		for (const page of pages) {
			const codeElems = page.document.querySelectorAll("pre code.hljs");
			if (codeElems.length < 1) {
				page.document.getElementById("syntax_css")?.remove();
			}
		}
	});

if (Deno.env.get("DENO_ENV") == "production") {
	site
		.process([".html"], async (pages) => {
			for (const page of pages) {
				page.content = await minify({
					compressor: htmlMinifier,
					content: page.content,
					options: {
						collapseWhitespace: true,
						collapseInlineTagWhitespace: false
					}
				});
			}
		})
		.process([".css"], async (pages) => {
			for (const page of pages) {
				page.content = await minify({
					compressor: cleanCSS,
					content: page.content
				});
			}
		})
		.process([".js"], async (pages) => {
			for (const page of pages) {
				page.content = await minify({
					compressor: terser,
					content: page.content
				});
			}
		})
}

export default site;

