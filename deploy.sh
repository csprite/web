#!/bin/bash

set -eu

COMMAND="${1:-deploy}"

if [ "$COMMAND" == "install-hook" ]; then
	ln -v $0 ".git/hooks/pre-push"
	exit 0
fi

# Check if there are commits to push: https://stackoverflow.com/a/48354942/14516016
if git merge-base --is-ancestor HEAD @{u}; then
	echo "Nothing to deploy"
	exit 0
fi

BUILD="./build/"
URL="https://csprite.github.io"

rm -rf $BUILD _data.json

echo -e "{ \"site_url\": \"$URL\" }" > _data.json
DENO_ENV=production deno task build --dest $BUILD --location "$URL"

# Push to Codeberg Pages
git clone https://github.com/csprite/csprite.github.io csp_source
cd csp_source/
git rm -r .
mv ../$BUILD/* .
rm -f *.md *.sh
git add .
git commit --amend -m "Deploy"
git push --force
cd ..
rm -rf csp_source/

rm -rf $BUILD _data.json

exit 0

