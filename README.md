This is source code of csprite blog, it is powered by deno + lume.

For deploying the blog a simple bash script `deploy.sh` is used, which builds
and deploys the blog to Cloudflare Pages, but I have included "code" for deploying
to other services like surge.sh or GitHub/Codeberg Pages, etc.

the deploy script can also install a hook which can deploy your site automatically
on pushing your code to remote.

