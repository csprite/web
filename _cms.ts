import lumeCMS from "lume/cms.ts";

const cms = lumeCMS();

cms.upload("media: image, video & etc files used by articles", "src:media")

cms.collection("articles: add, edit, or delete your articles", "src:posts/*.md", [
	"title: text",
	{
		name: "date",
		type: "date",
		label: "Published date",
		value: new Date(),
		attributes: {
			required: true
		}
	},
	{
		name: "last_modified_at",
		type: "date",
		label: "Last modified date",
		value: undefined
	},
	{
		name: "banner_image",
		type: "file",
		label: "Banner Image",
		value: undefined
	},
	{
		name: "draft",
		label: "Is Draft?",
		type: "checkbox",
		description: "If checked, the post will not be published."
	},
	{
		name: "tags",
		type: "list",
		label: "Tags",
		init(field) {
			const { data } = field.cmsContent;
			field.options = data.site?.search.values("tags");
		}
	},
	{
		name: "content",
		type: "markdown",
		label: "Content",
		attributes: {
			required: true
		}
	}
]);

export default cms;

